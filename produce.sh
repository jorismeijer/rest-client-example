#!/usr/bin/env bash
REST_HOST="https://192.168.99.100:18100"
JSON_HEADER="Content-Type: application/json"
APP_ID="rest.producer"
UUID="reporter-2"


if [[ $# -ne 2 ]]; then
    echo "Give two arguments <incidentId> <severity>"
    exit 1
fi

INCIDENT="$1"
SEVERITY="$2"

PRODUCE_TEMPLATE='
{
    "keyMessage" : {
        "type" : "STRING",
        "message" : ""
    },
    "valueMessage" : {
        "type" : "STRING",
        "message" : ""
    }
}'

JSON_STRING=$(jq --arg inc "$INCIDENT" --arg sev "$SEVERITY" '.incidentId = $inc | .severity = $sev' JSON/IncidentReport.json -c )
PAYLOAD=$(echo "$PRODUCE_TEMPLATE" | jq --arg key "$INCIDENT" --arg val "$JSON_STRING" '.keyMessage.message = $key | .valueMessage.message = $val')

echo
echo "Payload:"
echo
echo "$PAYLOAD" | jq

curl --request POST \
  --url "${REST_HOST}/stream/example/incidents" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "Content-Type: application/json" \
  --key /Users/Jumanne/Projects/axual/local/local-config/security/applications/example-producer/keys/example_producer.key \
  --cert /Users/Jumanne/Projects/axual/local/local-config/security/applications/example-producer/cer/example_producer.cer \
  --cacert /Users/Jumanne/Projects/axual/local/local-config/security/applications/common-truststore/cachain/common-truststore.pem \
  -s \
  --data "$PAYLOAD" | jq

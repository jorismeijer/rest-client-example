#!/usr/bin/env bash
REST_HOST="https://192.168.99.100:18100"
UUID="incident-consumer-1"

APP_ID="demo.incident.notifier"

LOOP="false"
if [[ $# -gt 0 ]] && [[ "$1" =~ l|Lo|Oo|Op|P ]];then
  LOOP="true"
  echo "Loop enabled, press CTRL-C to quit"
fi


function consume_messages {
  echo "Receiving"
  curl --request GET \
    --url "${REST_HOST}/stream/example/incidents" \
    --header "axual-application-id: $APP_ID" \
    --header 'axual-application-version: 1.0' \
    --header "axual-consumer-uuid: $UUID" \
    --header 'axual-key-type: STRING' \
    --header 'axual-value-type: STRING' \
    --header 'axual-commit-strategy: AFTER_READ' \
    --header 'axual-polling-timeout-ms: 10000' \
    --header 'axual-auto-offset-reset: EARLIEST' \
    --header 'Content-Type: application/json' \
    --key /Users/Jumanne/Projects/axual/local/local-config/security/applications/example-consumer/keys/example_consumer.key \
    --cert /Users/Jumanne/Projects/axual/local/local-config/security/applications/example-consumer/cer/example_consumer.cer \
    --cacert /Users/Jumanne/Projects/axual/local/local-config/security/applications/common-truststore/cachain/common-truststore.pem \
    -s | jq
}

if [[ "$LOOP" == "true" ]];then
  while [[ "0" == "0" ]]; do
    consume_messages
    echo "Looping, press CTRL-C to quit"
    sleep 2
  done
else
  consume_messages
fi

# REST Client example project

This project is to produce and consume messages locally from REST Proxy. The examples are verified to work with REST Proxy deployed in a local installation of Axual, version 2020.1.
For more info, check https://youtu.be/2iqh6h-q9wY?t=367. 
